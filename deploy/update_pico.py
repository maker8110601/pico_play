"""
A very rough update tool
"""
import sys
import pathlib
import subprocess

BASE_DIR = pathlib.Path(__file__).parent.parent
SRC_DIR = BASE_DIR / "src"

sys.path.append(str(SRC_DIR))


def update_pico():
    import magnet_detector
    import PiicoDev_QMC6310
    import PiicoDev_Unified

    for module in [magnet_detector, PiicoDev_QMC6310, PiicoDev_Unified]:
        mod_path = pathlib.Path(module.__file__).resolve()
        cmd = ["mpremote", "cp", str(mod_path), ":"]
        subprocess.run(cmd)
        # print(cmd)

    print(
        f'Run the project with: mpremote exec "import magnet_detector; magnet_detector.main()"'
    )

    print(
        f"The script will run until it ends.  If you disconnect, you can still reconnect "
        f"with: mpremote resume"
    )


if __name__ == "__main__":
    update_pico()
