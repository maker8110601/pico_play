======
README
======

The purpose of this repository is to understand the following questions:

Questions
=========

How do I SCM a MicroPython project?
-----------------------------------

This is not just about git

* How do I specify a dependencies file?
* Can I use poetry to help me manage it?

How do I develop and test for MicroPython
-----------------------------------------

What is IDE support like?

How do I QA MicroPython code?
-----------------------------

* What IDE tools are available for hinting / linting?

How do I deploy MicroPython project?
------------------------------------


How do I document a MicroPython project?
----------------------------------------

There is a tool called Fritzing which seems like a schematic editor,
although the website isn't clear.  When I have time I'll need to play
with it and see what it does.

Ezra has created a symbol set for PiicoDev here:

https://github.com/ezradevs/piicodev-symbols

A simple option that is familiar to me is Draw.IO.


The Project
===========

A simple tool that interacts with a PiicoDev magnetometer and updates
some RGBs to suit.


