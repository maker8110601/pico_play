========
Workflow
========

Concept
=======

As a simple step, what I am thinking:

* Use poetry for dev environment
* Tests can be written for basic QA of micropython files through syntax checking,
  cross-compile checking and so forth
* Write tools that performs manual packaging into a platform specific dist location
* Future:  Some kind of additional test / checklist / record of tests for hardware testing
  that is run on the destination hardware and configuration

MicroPython
===========

MicroPython needs to be flashed to the board:

https://docs.micropython.org/en/latest/rp2/quickref.html#installing-micropython

The following is also very important since the quickstart is quite vague.:

https://docs.micropython.org/en/v1.18/reference/mpremote.html

https://core-electronics.com.au/guides/raspberry-pi-pico-w-connect-to-the-internet/

# TODO:  Offer a PR to update the RPI2 docs.

Core electronics uses Thonny for MicroPython interaction.  PyCharms has a MicroPython plugin
that does not seem to work in the latest version.

To get connected::

    mpremote connect list

    # My computer looked like this
    # /dev/ttyACM0 e6614c311b813932 2e8a:0005 MicroPython Board in FS mode
    # /dev/ttyS0 None 0000:0000 None None

    # For REPL
    # mpremote connect /dev/ttyACM0

    # To see what can be imported
    mpremote exec 'help("modules")'

    # To see what is on the local FS
    mpremote fs ls /

    # Later on, local projects can be tested without copying to the device
    # simply by prefixing a command with the following (mounts the current
    # directory on the dev machine to the Pico which gets mounted as "/remote"

    mpremote mount .

Once done, a single file