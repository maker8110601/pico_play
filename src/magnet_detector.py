"""
A simple Python file for interacting with the magnetometer
"""

# Read the magnetic field strength and determine if a magnet is nearby

from PiicoDev_QMC6310 import PiicoDev_QMC6310
from PiicoDev_Unified import sleep_ms


def main():
    mag_sensor = PiicoDev_QMC6310(range=3000)  # initialise the magnetometer
    # magSensor.calibrate()

    threshold = 120  # microTesla or 'uT'.

    while True:
        strength = (
            mag_sensor.readMagnitude()
        )  # Reads the magnetic-field strength in microTesla (uT)
        my_string = (
            str(strength) + " uT"
        )  # create a string with the field-strength and the unit
        print(my_string)  # Print the field strength

        if strength > threshold:  # Check if the magnetic field is strong
            print("Strong Magnet!")

        sleep_ms(1000)


if __name__ == "__main__":
    main()
